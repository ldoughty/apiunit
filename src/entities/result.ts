import { IResult } from '../interfaces/iResult';
import { Diff } from './diff';

export class Result implements IResult
{
    testCaseName:string;
    diff:Diff[];
    get isPassed(): boolean {return !this.diff || this.diff.length === 0};

    constructor(testCaseName:string, diff:Diff[] = [])
    {
        this.testCaseName = testCaseName;
        this.diff = diff;
    }
};