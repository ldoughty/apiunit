import { IRequest } from '../interfaces/iRequest';
import { ApiTestRequest } from './api/apiTestRequest';
import { MsgTestRequest } from './msg/msgTestRequest';

export class RequestFactory
{
    static create(path:string): IRequest
    {
        if(path.endsWith('.req'))
        {
            return new ApiTestRequest(path);
        }
        else if(path.endsWith('.msg'))
        {
            return new MsgTestRequest(path);
        }
        else
        {
            throw new Error(`Unsupported request '${path}'`);
        }
    }
};