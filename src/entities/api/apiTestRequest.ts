import { IncomingHttpHeaders } from 'http';
import { IRequest as IHttpRequest, Request as HttpRequest } from 'http-file';
import client from 'sync-request';

import { IActualResponse } from '../../interfaces/iActualResponse';
import { ActualResponse } from '../actualResponse';
import { Request } from '../request';

export class ApiTestRequest extends Request
{
    private _request:IHttpRequest;

    private get request():IHttpRequest
    {
        return this._request || (this._request = HttpRequest.parse(this.rawRequest));
    }
    
    get shouldRetried():number
    {
        let retryHeaders = this.request.headers && Object.keys(this.request.headers).filter(h => h.toUpperCase() === 'APIUNIT-RETRY').map(h => this.request.headers[h]);

        if(!retryHeaders || retryHeaders.length === 0) return 0;

        if(retryHeaders[0] === 'true') return 15000;
        
        let waitFor = Number(retryHeaders[0]);

        if(Number.isNaN(waitFor)) return 0;

        return waitFor;
    }

    get maxRedirects(): number
    {
        let maxRedirects = this.request.headers && Object.keys(this.request.headers).filter(h => h.toUpperCase() === 'MAX-REDIRECTS').map(h => this.request.headers[h]);

        if(!maxRedirects || maxRedirects.length === 0) return 5;
        
        let result = Number(maxRedirects[0]);

        if(Number.isNaN(result)) return 0;

        return result;
    }

    constructor(file:string)
    {
        super(file);
    }

    execute(verbose?:boolean): Promise<IActualResponse>
    {
        return new Promise((resolve, reject) => 
        {
            try
            {
                let response = client(<"GET" | "HEAD" | "POST" | "PUT" | "DELETE" | "CONNECT" | "OPTIONS" | "TRACE" | "PATCH"> this.request.method, this.request.path, {headers: <IncomingHttpHeaders> this.request.headers, body: this.request.body, followRedirects: this.maxRedirects > 0, maxRedirects: this.maxRedirects });
                
                let result = new ActualResponse(response.statusCode, response.headers, response.body || '');
            
                resolve(result);
            }
            catch(error)
            {
                reject(error);
            }
        });
    }
};