import { spawnSync } from 'child_process';
import { lstatSync, readdirSync } from 'fs';
import { resolve } from 'path';
import { Dictionary } from 'typescript-collections';

import { IParameter } from '../interfaces/IParameter';
import { IResult } from '../interfaces/iResult';
import { ITestScenario } from '../interfaces/iTestScenario';
import { ITestSuite } from '../interfaces/iTestSuite';
import { Parameter } from './parameter';
import { TestSuite } from './testSuite';

let colors = require('colors/safe');
function sleep(ms:number): Promise<void>
{
    return new Promise((resolve, reject)=>setTimeout(resolve, ms));
}
export class TestScenario implements ITestScenario {
    name: string;
    readonly scenarioPath: string;
    defaultInputPath : string = '.';
    testSuites: Dictionary<string, ITestSuite>;
    input: string;
    requestDirName: string;
    responseDirName: string;
    paramsFile: string;
    beforeEachCommand: string;
    afterEachCommand: string;
    scenarios: string[];
    filePrefix: string;
    params: IParameter;
    verbose:boolean;

    private async getDirs(path: string): Promise<string[]> {
        const files = await readdirSync(path);
        let directories: string[] = [];
        for(const file of files){
            if(await lstatSync(resolve(path, file)).isDirectory()){
                directories.push(file);
            }
        }
        return directories;
    }

    constructor(input: string = '.', 
        paramsFile: string = 'parameters.json', 
        requestDirName: string = 'requests', 
        responseDirName: string = 'responses',
        filePrefix: string = '',
        scenarios: string[] = [],
        beforeEachCommand?: string,
        afterEachCommand?: string,
        verbose:boolean = false){
        this.input = input;
        this.requestDirName = requestDirName;
        this.responseDirName = responseDirName;
        this.paramsFile = paramsFile;
        this.params = new Parameter(paramsFile);
        this.beforeEachCommand = beforeEachCommand;
        this.afterEachCommand = afterEachCommand;
        this.scenarios = scenarios;
        this.filePrefix = filePrefix;
        this.testSuites = new Dictionary<string, ITestSuite>();
        this.verbose = verbose;
    }

    async setup(path?: string): Promise<void> {
        if(!path){
            path = this.input;
        }
        const directories = await this.getDirs(path);
        if(directories.some(d => d === this.requestDirName) 
            && directories.some(d => d === this.responseDirName)){
                const scnParams = new Parameter(this.paramsFile);
                let groups = scnParams.getScenarioGroups(path);
                if(scnParams.getScenarioGroups(path).length > 0) {
                    for (const group of groups){
                        let groupParams = scnParams.getScenarioParams(group);
                        this.testSuites.setValue(`${path}-${group}`,
                            new TestSuite(
                                `${path}/${this.requestDirName}`,
                                `${path}/${this.responseDirName}`,
                                groupParams,
                                this.filePrefix
                        ));
                    }
                }else{
                    this.testSuites.setValue(path === this.defaultInputPath? 'default': path,
                        new TestSuite(
                            `${path}/${this.requestDirName}`,
                            `${path}/${this.responseDirName}`,
                            scnParams,
                            this.filePrefix
                    ));
                }
        }
        
        for(const directory of directories){
            if(directory !== this.requestDirName && directory !== this.responseDirName){
                await this.setup(`${path}/${directory}`);
            }
        }
    }

    testsToRun(): string[] {
        let keysToTest: string[] = [];
        let matched: string[] = [];
        
        if(this.scenarios.length > 0){
            for(const key of this.testSuites.keys()){
                let keyPaths = key.split('/');
                for(const path of keyPaths){
                    if(this.scenarios.some( s => s === path)){
                        if(keysToTest.indexOf(key) === -1){
                            keysToTest.push(key);
                            matched.push(this.scenarios.find( s => s === path));
                        }
                    }
                }
            }
        }else{
            keysToTest = this.testSuites.keys();
        }

        if(matched.length !== this.scenarios.length){
            for(const scenario of this.scenarios){
                if(matched.indexOf(scenario) === -1){
                    console.log(colors.yellow(`Warning: Requested scenario '${scenario}' does not exist.`));
                }
            }
        }

        return keysToTest;
    }

    private toReadableScenario(input: string): string {
        return input.replace('./','').replace('.','').split('/').join(' - ');
    }
    async runSuites(): Promise<number> {
        let numFailed : number = 0;
        const testsToRun : string[] = this.testsToRun();
        let testResults: Dictionary<string, IResult[]> = new Dictionary<string, IResult[]>();

        if(this.scenarios.length > 0 && testsToRun.length === 0){
            console.log(colors.red(`Error: Non-existent scenario${this.scenarios.length > 1? 's': ''} requested!`));
            return;
        }
        console.log(colors.white(`Running ${testsToRun.length} scenario${testsToRun.length > 1 ? 's': ''}.\n\n`));
        for(let suiteKey of testsToRun){
            const suite = this.testSuites.getValue(suiteKey);
            
            if(this.beforeEachCommand){
                const commands = this.beforeEachCommand.split(' ');
                await spawnSync(commands[0],[...commands.splice(1), suiteKey],this.verbose?({stdio: [process.stdin, process.stdout, process.stderr]}):undefined);
            }

            console.log(colors.white(`Starting scenario '${this.toReadableScenario(suiteKey)}'\n\n`));            
            let failedResults:IResult[] = [];
            suite.on('passed', (result:IResult) =>
            {
                console.log(` ${colors.green('✓')} ${colors.gray(result.testCaseName)}`);
            });

            suite.on('failed', (result:IResult) =>
            {
                failedResults.push(result);
                console.log(colors.red(` ${failedResults.length}) ${result.testCaseName}`));
            });        

            await suite.execute(this.verbose);


            testResults.setValue(suiteKey, failedResults);

            let failedCount = 1;
            for(let failedResult of failedResults) {
                console.log(colors.white(`${failedCount}) ${failedResult.testCaseName}`));
                failedCount++;
                for(let diff of failedResult.diff) {
                    console.log(colors.red(`    ${diff.message}`));
                }
            }

            console.log(colors.green(`Passed. (${suite.testCases.size() - failedResults.length})\n`));
            if(failedResults.length !== 0 && suite.testCases.size() > 0){
                console.log(colors.red(`Failed. (${failedResults.length})\n`));
                numFailed += failedResults.length;
            }
            else
            {
                console.log(colors.green(`Tests passed.`));
            }
            if(this.afterEachCommand){
                const commands = this.afterEachCommand.split(' ');
                await spawnSync(commands[0],[...commands.splice(1)],this.verbose?({stdio: [process.stdin, process.stdout, process.stderr]}):undefined);
            }

            await sleep(1000);
        }

        for(const key of testResults.keys()){
            if(testResults.getValue(key).length === 0){
                console.log(` ${colors.green('✓')} ${colors.gray(key)}`);
            }else{
                console.log(colors.red(` ❌ ${this.toReadableScenario(key)} (${testResults.getValue(key).length})`));
            }
        }

        return numFailed;
    }
}