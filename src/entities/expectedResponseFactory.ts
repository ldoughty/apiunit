import { IExpectedResponse } from '../interfaces/iExpectedResponse';
import { ExpectedResponse } from './expectedResponse';

export class ExpectedResponseFactory
{
    static create(path:string): IExpectedResponse
    {
        if(path.endsWith('.resp'))
        {
            return new ExpectedResponse(path); 
        }
        if(path.endsWith('.msg'))
        {
            return new ExpectedResponse(path); 
        }
        else
        {
            throw new Error(`Unsupported response '${path}'`);
        }
    }
};