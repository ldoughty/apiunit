import { Dictionary } from 'typescript-collections';

import { ITestCase } from '../interfaces/iTestCase';

import { parse as parseXml } from 'fast-xml-parser';

let jpath = require('json-path')

export function injectParameters(text:string, parameters:{}): string
{
    for(let parameter in parameters)
    {
        text = text.replace(new RegExp('\\${' + parameter + '}', 'g'), parameters[parameter]);
    }
    return text;
}
    
export function injectDynamicParameters(testCases:Dictionary<string, ITestCase>, text:string) : string
{
    for (let testCaseName of testCases.keys()) 
    {
        let regex = new RegExp('\\$\{' + testCaseName + '\:([^\}]+)\}');
        let matches = regex.exec(text);

        while(matches)
        {
            let jsonPath = matches[1];
            let json:string = undefined;

            try
            {
                const testResponse = testCases.getValue(testCaseName).actualResponse;
                json = (/^(text|application)\/xml/.test(testResponse.headers?.['content-type']))
                    ? parseXml(testResponse.body)
                    : JSON.parse(testResponse.body);
            }
            catch(error)
            {
                break;
            }

            let data = jpath.resolve(json, jsonPath);
            
            if(data)
            {
                text = text.replace(regex, data[0]);
            }

            matches = regex.exec(text);
        }
    }

    return text;
}
