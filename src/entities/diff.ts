import { IDiff } from '../interfaces/iDiff';

export class Diff implements IDiff
{
    message:string;

    constructor(message:string)
    {
        this.message = message;
    }
};