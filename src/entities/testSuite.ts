import { EventEmitter } from 'events';
import * as fs from 'fs';
import * as path from 'path';
import { Dictionary } from 'typescript-collections';

import { IParameter } from '../interfaces/IParameter';
import { IResult } from '../interfaces/iResult';
import { ITestCase } from '../interfaces/iTestCase';
import { ITestSuite } from '../interfaces/iTestSuite';
import { ExpectedResponseFactory } from './expectedResponseFactory';
import { RequestFactory } from './RequestFactory';
import { TestCase } from './testCase';

export class TestSuite extends EventEmitter implements ITestSuite
{
    testCases:Dictionary<string, ITestCase>;
    requestPath:string;
    expectedResponsePath:string;
    parameters:object;
    filePrefix:string;

    constructor(requestPath:string, expectedResponsePath:string, fileParameters?:IParameter, filePrefix?:string)
    {
        super();

        this.testCases = new Dictionary<string, ITestCase>();
        this.requestPath = requestPath;
        this.expectedResponsePath = expectedResponsePath;
        this.filePrefix = filePrefix;
        this.parameters = fileParameters? fileParameters.parameters: {}

        if(!fs.existsSync(this.requestPath))
        {
            throw new Error(`${this.requestPath} does not exists.`);
        }

        this.loadTestCases();
    }

    private loadTestCases(): void
    {
        let pathInfo = fs.lstatSync(this.requestPath);
        
        if(pathInfo.isDirectory())
        {
            let files = fs.readdirSync(this.requestPath);
            
            for(let file of files)
            {
                if(this.filePrefix === undefined || file.startsWith(this.filePrefix))
                {
                    let testCase = this.getTestCase(path.join(this.requestPath, file));
                    this.testCases.setValue(testCase.name, testCase);
                }
            }
        }
        else if(pathInfo.isFile())
        {
            if(this.filePrefix === undefined || this.requestPath.startsWith(this.filePrefix))
            {
                let testCase = this.getTestCase(this.requestPath);
                this.testCases.setValue(testCase.name, testCase);
            }
        }
    }

    private getTestCase(requestFilePath:string): ITestCase
    {
        let requestFileName = path.basename(requestFilePath);
        let requestFileExtension = path.extname(requestFilePath);
        let responseFileName = requestFileName;

        if(requestFileExtension === '.req')
        {
            responseFileName = path.basename(requestFileName, '.req') + '.resp';
        }
        else if ( requestFileExtension !== '.msg' )
        {
            throw new Error(`Unsupported file extension '${requestFileExtension}' provided. Please use one of [ '.msg', '.req' ].`)
        }

        let responseFilePath = path.join(this.expectedResponsePath, responseFileName);

        // We want to lstatSync before building the test case, else we won't know if a file doesn't exist until after run-time.
        // This also throws errors instead of silently failing.
        fs.lstatSync(responseFilePath)
        let testCase = new TestCase(RequestFactory.create(requestFilePath), ExpectedResponseFactory.create(responseFilePath));
        testCase.suite = this;
        return testCase;
    }

    public async execute(verbose?:boolean): Promise<IResult[]>
    {
        let results:IResult[] = [];

        for(let testCaseName of this.testCases.keys())
        {
            let testCase = this.testCases.getValue(testCaseName);

            let result = await testCase.execute(verbose);

            this.emit(result.isPassed ? 'passed' : 'failed', result);

            results.push(result);
        }

        return results;
    }
};