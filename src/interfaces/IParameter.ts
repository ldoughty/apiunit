export interface IParameter {
    parameters: { [key : string]: string | object };
    setParam(paramName: string, value: string): void;
    getScenarioParams(scenarioName: string): IParameter;
    getScenarioGroups(scenarioName: string): string[];
}