import { IActualResponse } from './iActualResponse';
import { IDiff } from './iDiff';
import { IExpectedResponse } from './iExpectedResponse';
import { IRequest } from './iRequest';
import { IResult } from './iResult';
import { ITestSuite } from './iTestSuite';

export interface ITestCase
{
    diff:IDiff[];
    suite:ITestSuite;
    readonly name:string;
    readonly requestFilePath:string
    readonly expectedResponseFilePath:string;

    request:IRequest;
    actualResponse:IActualResponse;
    expectedResponse:IExpectedResponse;
    
    execute(verbose:boolean): Promise<IResult>;
}