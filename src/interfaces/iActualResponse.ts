import { IDiff } from './iDiff';
import { IExpectedResponse } from './iExpectedResponse';

export interface IActualResponse
{
    statusCode:number;
    headers:{};
    body:any;
    diff(expectedResponse:IExpectedResponse): IDiff[]
};