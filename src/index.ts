import { TestScenario } from './entities/testScenario';
export * from './entities/RequestFactory';
export * from './entities/actualResponse';
export * from './entities/diff';
export * from './entities/expectedResponse';
export * from './entities/expectedResponseFactory';
export * from './entities/helpers';
export * from './entities/parameter';
export * from './entities/request';
export * from './entities/result';
export * from './entities/testCase';
export * from './entities/testScenario';
export * from './entities/testSuite';

export * from './interfaces/IParameter';
export * from './interfaces/iActualResponse';
export * from './interfaces/iDiff';
export * from './interfaces/iExpectedResponse';
export * from './interfaces/iRequest';
export * from './interfaces/iRequestRepository';
export * from './interfaces/iResult';
export * from './interfaces/iTestCase';
export * from './interfaces/iTestScenario';
export * from './interfaces/iTestSuite';

let options = require('commander');
let colors = require('colors/safe');
let packageJson = require('../package.json');

let testPassed = true;
let pastResponses = {};

function error(message:string | NodeJS.ErrnoException): void
{
    if(message)
    {
        console.log('\n\n',message);
        process.exit(1);
    }
}

function warn(message:string): void
{
    console.log('   ',message);
    testPassed = false;
}


async function main()
{
    options.option('-i, --input <path>', 'A directory containing request files.')
        .option('-d, --diff <path>', 'A directory containing expected outputs.')
        .option('-v, --verbose', 'Verbose mode')
        .option('-p, --parameter <path>', 'A parameter file')
        .option('-x, --prefix <prefix>', 'Prefix of request files to include in this test')
        .option('-b, --before-each-cmd <command to run>', 'Command to run before every scenario.')
        .option('-a, --after-each-cmd <command to run>', 'Command to run after each scenario.')
        .option('-c, --cmd <cmd>', 'Deprecated. Use --after-each-cmd instead.')
        .option('-s, --scn <scenario>', 'The scenario, or comma seperated scenarios to run.')
        .parse(process.argv);

    if (!options.input)
    {
        error('A directory containing request files must be specified.');
    }
    if (!options.diff)
    {
        error('A directory containing expected outputs must be specified.');
    }

    let input = options.input;
    let output = options.diff;
    let parameterFile = options.parameter;
    let filePrefix = options.prefix;
    let scenariosToRun = options.scn;
    let beforeEachCommand = options.beforeEachCmd;
    let afterEachCommand = options.afterEachCmd
    if (!afterEachCommand && options.cmd)
    {
        console.log(
            '********************************************************************************************************************************************\n' +
            '* "cmd" is an obsolete parameter and will be removed in future versions.  Use "after-each-cmd" instead.\n' +
            '********************************************************************************************************************************************\n'
        );
        afterEachCommand = options.cmd;
    }
    let verbose = options.verbose;
    console.log(`\n\n${packageJson.name} - ${packageJson.version}\n\n`);
    if(beforeEachCommand){
        console.log(`${beforeEachCommand} will be run before each scenario.`);
    }
    if(afterEachCommand){
        console.log(`${afterEachCommand} will be run after each scenario.`);
    }

    let scenario = new TestScenario(undefined, parameterFile, input, output, filePrefix, scenariosToRun? scenariosToRun.split(','): scenariosToRun, beforeEachCommand, afterEachCommand, verbose);
    await scenario.setup();
    const failCount = await scenario.runSuites();
    process.exit(failCount);
}

main().catch((e)=>{ console.log(e); process.exit(1); });