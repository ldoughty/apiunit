import { assert } from 'chai';
import { Mock } from 'typemoq';

import { ActualResponse } from '../src/entities/actualResponse';
import { Diff } from '../src/entities/diff';
import { Request } from '../src/entities/request';
import { TestCase } from '../src/entities/testCase';
import { IActualResponse } from '../src/interfaces/iActualResponse';
import { IExpectedResponse } from '../src/interfaces/iExpectedResponse';

class FakeActualTestResponse extends ActualResponse
{
    _diff:Diff[];

    constructor(diff:Diff[])
    {
        super(undefined, undefined, undefined);
        this._diff = diff;
    }

    diff(expectedResponse:IExpectedResponse)  {
        return this._diff;
    }
};

class FakeTestRequest extends Request
{
    requestPath:string;
    actualTestResponse:IActualResponse;

    constructor(actualTestResponse:IActualResponse, requestPath:string)
    {
        super(requestPath);
        this.actualTestResponse = actualTestResponse;
    }

    async execute(): Promise<IActualResponse> 
    {
        return new Promise<IActualResponse>((resolve,reject) =>
        {
            return resolve(this.actualTestResponse);
        });
    }
};

describe('TestCase', () => 
{ 
    it('Expose properties properly', () => 
    {
        let mockedExpectedResponse = Mock.ofType<IExpectedResponse>();
        mockedExpectedResponse.setup((r)=>r.responsePath).returns(()=>'res_path.resp');

        let mockedRequest = Mock.ofType<Request>();
        mockedRequest.setup((r)=>r.requestPath).returns(()=>'req_path.req');

        let testCase = new TestCase(mockedRequest.object, mockedExpectedResponse.object);

        assert.equal(testCase.requestFilePath, 'req_path.req');
        assert.equal(testCase.expectedResponseFilePath, 'res_path.resp');
        assert.equal(testCase.name, 'req_path');
    });

    
    it('Execute properly', async () => 
    {
        let diff = [new Diff('msg')];
        
        let mockedExpectedResponse = Mock.ofType<IExpectedResponse>();

        let mockedActualResponse = new FakeActualTestResponse(diff);

        let mockedRequest = new FakeTestRequest(mockedActualResponse, 'name.req');

        let testCase = new TestCase(mockedRequest, mockedExpectedResponse.object);

        let result = await testCase.execute();
        
        assert.equal(result.testCaseName, 'name');
        assert.isFalse(result.isPassed);
        assert.equal(result.diff.length, 1);
        assert.equal(result.diff[0].message, 'msg');
        assert.equal(testCase.actualResponse, mockedActualResponse);
    });
});