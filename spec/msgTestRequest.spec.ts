import { assert } from 'chai';
import * as TypeMoq from 'typemoq';
import { Dictionary } from 'typescript-collections';

import { ActualResponse } from '../src/entities/actualResponse';
import { MsgTestRequest } from '../src/entities/msg/msgTestRequest';
import { ITestCase } from '../src/interfaces/iTestCase';
import { ITestSuite } from '../src/interfaces/iTestSuite';


class FakeActualResponse extends ActualResponse
{
    constructor(body:any, statusCode:number = 200, headers:{} = {})
    {
        super(statusCode, headers, body);
    }
};

describe('MsgTestRequest', () => 
{ 
    it('Load a test request properly', async () => 
    {
        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({'PARAM1': 'VALUE1'}));
        testSuite.setup((s)=>s.testCases).returns(()=>new Dictionary<string, ITestCase>());

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);

        process.env.AWS_REGION = 'mars-north-1'

        let req = new MsgTestRequest('./assets/testRequest/msg/request.msg');
        req.testCase = testCase.object;

        assert.equal(req.responseQueue, "protocol://queue/url/here");
        assert.equal(req.request.method, "PUSH");
        assert.equal(req.request.path, "/some/path");
        assert.equal(req.request.body, "Hi there");
        assert.equal(req.request.headers['x-header'], 'x-value');

        assert.equal(req.awsRegion, 'mars-north-1');
    });

    it('Inject static parameters properly', async () => 
    {
        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({'PARAM1': 'VALUE1', 'PARAM3': 'VALUE3'}));
        testSuite.setup((s)=>s.testCases).returns(()=>new Dictionary<string, ITestCase>());

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        process.env.AWS_REGION = 'mars-north-1'; //To be overridden by a header in the msg file.

        let req = new MsgTestRequest('./assets/testRequest/msg/request_with_params.msg');
        req.testCase = testCase.object;

        assert.equal(req.responseQueue, "protocol://queue/url/here");
        assert.equal(req.request.method, "PUSH");
        assert.equal(req.request.path, "/some/path");
        assert.equal(req.request.body, "VALUE1\n\n${PARAM2}\n\nVALUE3");
        assert.equal(req.request.headers['aws-region'], 'us-east-1');
        assert.equal(req.request.headers['x-header'], 'x-value');

        assert.equal(req.awsRegion, 'us-east-1');
    });
    
    it('Inject dynamic parameters properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<ITestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('{"property1": "Hi there", "property2": 42 }'));

        let testCases = new Dictionary<string, ITestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let req = new MsgTestRequest('./assets/testRequest/msg/request_with_dynamic_params.msg');
        req.testCase = testCase.object;

        assert.equal(req.responseQueue, "protocol://queue/url/here");
        assert.equal(req.request.method, "PUSH");
        assert.equal(req.request.path, "/some/path");
        assert.equal(req.request.body, "Hi there 42\nand undefined ${test-2:/property1}");
        assert.equal(req.request.headers['aws-region'], 'us-west-1');
        assert.equal(req.request.headers['x-header'], 'x-value');

        assert.equal(req.awsRegion, 'us-west-1');
    });

    it('Inject dynamic parameters - handle non-json response properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<ITestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('This is not a json file.'));

        let testCases = new Dictionary<string, ITestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let req = new MsgTestRequest('./assets/testRequest/msg/request_with_dynamic_params_and_non_json_response.msg');
        req.testCase = testCase.object;
        
        assert.equal(req.responseQueue, "protocol://queue/url/here");
        assert.equal(req.request.method, "PUSH");
        assert.equal(req.request.path, "/some/path");
        assert.equal(req.request.body, "${test-1:/property1}");
        assert.equal(req.request.headers['aws-region'], 'ap-south-1');
        assert.equal(req.request.headers['x-header'], 'x-value');

        assert.equal(req.awsRegion, 'ap-south-1');
    }); 
});