import { assert } from 'chai';
import * as TypeMoq from 'typemoq';
import { Dictionary } from 'typescript-collections';

import { ActualResponse } from '../src/entities/actualResponse';
import { ExpectedResponse } from '../src/entities/expectedResponse';
import { ITestCase } from '../src/interfaces/iTestCase';
import { ITestSuite } from '../src/interfaces/iTestSuite';


class FakeActualResponse extends ActualResponse
{
    constructor(body:any, statusCode:number = 200, headers:{} = {})
    {
        super(statusCode, headers, body);
    }
};


class FakeExpectedResponse extends ExpectedResponse
{
    constructor(path:string)
    {
        super(path);
    }
};

describe('ExpectedResponse', () => 
{ 
    it('Load an expected response properly', async () => 
    {
        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({'PARAM1': 'VALUE1'}));
        testSuite.setup((s)=>s.testCases).returns(()=>new Dictionary<string, ITestCase>());

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let resp = new FakeExpectedResponse('./assets/testResponse/response.resp');
        resp.testCase = testCase.object;

        assert.equal(resp.body, "Hi there");
    });

    it('Inject static parameters properly', async () => 
    {
        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({'PARAM1': 'VALUE1', 'PARAM3': 'VALUE3'}));
        testSuite.setup((s)=>s.testCases).returns(()=>new Dictionary<string, ITestCase>());

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let resp = new FakeExpectedResponse('./assets/testResponse/response_with_params.resp');
        resp.testCase = testCase.object;

        assert.equal(resp.body, "VALUE1\n\n${PARAM2}\n\nVALUE3");
    });
    
    it('Inject dynamic parameters properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<ITestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('{"property1": "Hi there", "property2": 42 }'));

        let testCases = new Dictionary<string, ITestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let resp = new FakeExpectedResponse('./assets/testResponse/response_with_dynamic_params.resp');
        resp.testCase = testCase.object;

        assert.equal(resp.body, "Hi there 42\nand undefined ${test-2:/property1}");
    });

    it('Inject dynamic parameters - handle non-json response properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<ITestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('This is not a json file.'));

        let testCases = new Dictionary<string, ITestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<ITestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<ITestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let resp = new FakeExpectedResponse('./assets/testResponse/response_with_dynamic_params_and_non_json_response.resp');
        resp.testCase = testCase.object;
        
        assert.equal(resp.body, "${test-1:/property1}");
    });
});