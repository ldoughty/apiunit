import { assert } from 'chai';

import { TestScenario } from '../src/entities/testScenario';

describe('Test Scenario', async () => 
{ 
    function setup(sourceDir: string): TestScenario 
    {
        let scenario = new TestScenario(sourceDir,'assets/parameters.json', 'requests','responses', '',[],'echo "echo echo echo!"');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'requests');
        assert.equal(scenario.responseDirName, 'responses');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.filePrefix, '');
        assert.equal(scenario.beforeEachCommand, 'echo "echo echo echo!"');
        assert.equal(scenario.testSuites.size(), 0);
        return scenario;
    }

    it('Create a default scenario', async () => 
    {
        let scenario = setup('assets/testSuite');
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), ['assets/testSuite']);
        assert.equal(scenario.testSuites.size(), 1);
        assert.equal(scenario.testsToRun().length, 1);
    });

    it('Create a complex scenario', async () => 
    {

        let scenario = setup('assets/testScenario');
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), [ 'assets/testScenario',
            'assets/testScenario/testScenario',
            'assets/testScenario/testScenario/testScenarioTestScenario' ]);
        assert.equal(scenario.testSuites.size(), 3);
        assert.equal(scenario.testsToRun().length, 3);
    });
    
    it('Create a complex scenario with only 1 scenario to run', async () => 
    {
        let scenario = setup('./assets/testScenario');
        scenario.scenarios = ['testScenario2'];
        await scenario.setup();
        assert.equal(scenario.testSuites.size(), 3);
        assert.equal(scenario.testsToRun().length, 0);
    });
    
    it('Create a complex scenario with only 1 scenario to run', async () => 
    {
        let scenario = new TestScenario('assets','assets/parameters.json', 'requestz','responsez', undefined, ['testScenario2']);
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'requestz');
        assert.equal(scenario.responseDirName, 'responsez');
        assert.deepEqual(scenario.scenarios, ['testScenario2']);
        assert.equal(scenario.testSuites.size(), 0);
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), [ 'assets/testScenario/testScenario2' ]);
        assert.equal(scenario.testSuites.size(), 1);
        assert.equal(scenario.testsToRun().length, 1);
    });
    
    it('Create a complex scenario with only 3 scenarios to run ( 3 nested )', async () => 
    {
        let scenario = setup('assets');
        scenario.scenarios = ['testScenario'];
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), [ 'assets/testScenario',
            'assets/testScenario/testScenario',
            'assets/testScenario/testScenario/testScenarioTestScenario' ]);
        assert.equal(scenario.testSuites.size(), 4);
        assert.equal(scenario.testsToRun().length, 3);
    });
    
    it('Create a complex scenario with only many to run', async () => 
    {
        let scenario = new TestScenario('assets','assets/parameters.json', 'requestz','responsez');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'requestz');
        assert.equal(scenario.responseDirName, 'responsez');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), [ 'assets/testScenario/testScenario2' ]);
        assert.equal(scenario.testSuites.size(), 1);
        assert.equal(scenario.testsToRun().length, 1);
    });

    it('Create a scenario with .msg files', async () => 
    {
        let scenario = new TestScenario('assets/testRequest','assets/parameters.json', 'msg','msg');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'msg');
        assert.equal(scenario.responseDirName, 'msg');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), [ 'assets/testRequest' ]);
        assert.equal(scenario.testSuites.size(), 1);
        assert.equal(scenario.testsToRun().length, 1);
    });

    it('Create a scenario with different folder names - throws invalid response', async () => 
    {
        let scenario = new TestScenario('assets','assets/parameters.json', 'api','msg');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'api');
        assert.equal(scenario.responseDirName, 'msg');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        try
        {
            await scenario.setup();
            assert.isTrue(false);
        }
        catch(e)
        {
            assert.isTrue(e.message.includes("no such file or directory, lstat 'assets/testRequest/msg/request.resp'"));
            assert.isTrue(true);
        }
        assert.deepEqual(scenario.testsToRun(), []);
        assert.equal(scenario.testSuites.size(), 0);
        assert.equal(scenario.testsToRun().length, 0);
    });

    it('Create a scenario with different folder names - throws invalid request', async () => 
    {
        let scenario = new TestScenario('assets/invalidScenario','assets/parameters.json', 'reqs', 'resps');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'reqs');
        assert.equal(scenario.responseDirName, 'resps');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        try
        {
            await scenario.setup();
            assert.isTrue(false);
        }
        catch(e)
        {
            assert.isTrue(e.message.includes("Unsupported file extension '.invalid' provided. Please use one of [ '.msg', '.req' ]."));
            assert.isTrue(true);
        }
        assert.deepEqual(scenario.testsToRun(), []);
        assert.equal(scenario.testSuites.size(), 0);
        assert.equal(scenario.testsToRun().length, 0);
    });

    it('Create a scenario with different folder names', async () => 
    {
        let scenario = new TestScenario('assets','assets/parameters.json', 'diffReqs','diffResps');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'diffReqs');
        assert.equal(scenario.responseDirName, 'diffResps');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), [ 'assets/diffReqRespDirScenario' ]);
        assert.equal(scenario.testSuites.size(), 1);
        assert.equal(scenario.testsToRun().length, 1);
    });

    it('Create a scenario with incorrect folder names', async () => 
    {
        let scenario = new TestScenario('assets','assets/parameters.json', 'apk','msg');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'apk');
        assert.equal(scenario.responseDirName, 'msg');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), []);
        assert.equal(scenario.testSuites.size(), 0);
        assert.equal(scenario.testsToRun().length, 0);
    });

    it('Create a scenario with incorrect base directory', async () => 
    {
        let scenario = new TestScenario('assetz','assets/parameters.json', 'api','msg');
        assert.equal(scenario.paramsFile, 'assets/parameters.json');
        assert.equal(scenario.requestDirName, 'api');
        assert.equal(scenario.responseDirName, 'msg');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.testSuites.size(), 0);
        let threwError = false;
        try{
            await scenario.setup();
        }catch(e){
            threwError = true;
        }
        assert.isTrue(threwError);
        assert.deepEqual(scenario.testsToRun(), []);
        assert.equal(scenario.testSuites.size(), 0);
        assert.equal(scenario.testsToRun().length, 0);
    });
});