# Apiunit

Apiunit is an awesome api unit testing framework! 
It allows creation of http requests and responses to create unit tests. 

# Commands

--input (-i): path to directory containing inputs (/requests is default)
--diff (-d): path to directory containing expected output (/responses is default)
--parameter (-p): path to the parameter .json file
--prefix (-x): prefix of request(input) files to run
--cmd (-c): command to be run between scenarios
--scn (-s): The scenario or comma separated scenarios to run.

## Parameter file example
Parameter files should be JSON format. Here is an example:
<pre>
{
   "PARAM1":"VALUE1",
   "PARAM2":"VALUE2",
   "settings":{
      "scenarios":{
         "scn1":{
            "PARAM1":"scn1",
            "PARAM2":"scn1"
         },
         "scn2":{
            "PARAM1":"scn2",
            "PARAM2":"scn2"
         }
      },
      "groups":{
         "assets/testSuite":[
            "scn1",
            "scn2"
         ]
      }
   }
}
</pre>

In this example we are running two scenarios named **scn1** and **scn2** on the folder(group) of "assets/testSuite". In each scenario, the **PARAM1** and **PARAM2** variables will be replaced with the values in the scenarios.

## Request / Response file examples
Request files should end with **.req or .msg** extension, and response files should end with **.resp** extension. Every request should have a matching response with the **same** name minus extension. Response files can also use regular expressions to match expected responses.

Request files also allow the parameter **APIUNIT-RETRY** which can be set to any value, but default is **15000ms**. **APIUNIT-RETRY** will retry the request until it matches the associated response or fails within the given timeframe.

Response files also allow the header **apiunit-ignore-array-order** which use a JSONPath expression to allow arrays to be unordered. Use `path1,path2` where the value is a comma separated list of expressions to allow. Any array can be represented with `$..`, or a more explicit path can be specified. For example, targeting `values` in only `key1` in the following json structure would look like `data[*].key1.values`.
```json
{
   "data": [
      {
         "key1": {
            "values": [1,2,3]
         },
         "key2": {
            "values": [4,5,6]
         }
      },
      {
         "key1": {
            "values": [1,2,3]
         },
         "key2": {
            "values": [4,5,6]
         }
      }
   ]
}
```

Example request:
<pre>
GET ${some-domain-name-from-parameters}/some-path/${some-id-from-previous-test-or-parameters}
</pre>

Example response:
<pre>
200
content-type: application/json

{
	"maybe-an-id":".+",
	"some-field":"some-value",
	"another-field":"another-value"
}
</pre>

## Example Execution
To run a simple apiunit task just run the command "node ../node_modules/.bin/apiunit -i specs/requests -d specs/responses -p parameters.json -c \"npm run add-some-data-or-something\"".